
console.log("Welcome to JS");


function helloMrAlert(name) {
    var justANumber = 42;
    alert("Hello " + name + ", here is a number: " + justANumber);
}

function helloMrConsole(name) {
    var thisBeHereResult = 3 + 42;
    console.log("Hello " + name + ", here is a number: " + thisBeHereResult);
}

function triggerMagic(type) {
    var name = document.getElementById("name").value;
    if (type == "alert") {
        helloMrAlert(name);
    } else {
        helloMrConsole(name);
    }
}

console.log("JS Parsed");

