package edu.ktu.myuniproject.repository;

import edu.ktu.myuniproject.domain.TodoItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public interface TodoRepository extends JpaRepository<TodoItem, Long> {
}
