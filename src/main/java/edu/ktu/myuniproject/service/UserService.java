package edu.ktu.myuniproject.service;

import edu.ktu.myuniproject.domain.User;
import edu.ktu.myuniproject.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  private final UserRepository repository;

  @Autowired
  public UserService(UserRepository repository) {
    this.repository = repository;
  }

  public List<User> findAll() {
    return repository.findAll();
  }

  public void create(User user) {
    repository.save(user);
  }

  public void delete(Long id) {
    repository.deleteById(id);
  }

}
