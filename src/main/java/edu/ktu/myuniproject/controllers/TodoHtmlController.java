package edu.ktu.myuniproject.controllers;

import edu.ktu.myuniproject.domain.TestBody;
import edu.ktu.myuniproject.domain.TodoItem;
import edu.ktu.myuniproject.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/html/todo")
public class TodoHtmlController {

    @Autowired
    private TodoService service;

    @GetMapping(value = "/all")
    public String all(Model model) {
        List<TodoItem> items = service.findAll();
        model.addAttribute("todos", items);

        return "funstuff";
    }

    @PostMapping(value = "/test", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String testSubmit(@RequestParam Map<String, String> datamap, Model model) {
        TestBody body = new TestBody();
        body.space = (String) datamap.get("space");
        body.kittens = (String) datamap.get("kittens");

        model.addAttribute("testbody", body);

        return "submitfun";
    }

}
