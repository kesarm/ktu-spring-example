package edu.ktu.myuniproject.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TodoItemTest {

    @Test
    public void testEqualsSameObject() {
        TodoItem todoItem = new TodoItem();
        assertTrue(todoItem.equals(todoItem));
    }

    @Test
    public void testEqualsDifferentClass() {
        TodoItem todoItem = new TodoItem();
        assertFalse(todoItem.equals("TodoItem"), "Different class");
    }

    @Test
    public void testEqualsEmptyObject() {
        TodoItem todoItem = new TodoItem();
        assertTrue(todoItem.equals(new TodoItem()));
    }
}