package edu.ktu.myuniproject.service;

import edu.ktu.myuniproject.domain.TodoItem;
import edu.ktu.myuniproject.domain.TodoPriority;
import edu.ktu.myuniproject.domain.User;
import edu.ktu.myuniproject.repository.TodoRepository;
import edu.ktu.myuniproject.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class TodoServiceTest {

    private User user;
    private TodoItem todoItem;
    private TodoRepository todoRepository;
    private TodoService todoService;

    @BeforeEach
    void setUp() {
        user = new User(1L, "Martynas");
        todoItem = new TodoItem("Wash dishes", TodoPriority.LOW, user);
        UserRepository userRepository = mock(UserRepository.class);
        when(userRepository.findById(any())).thenReturn(Optional.empty());
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        todoRepository = mock(TodoRepository.class);
        todoService = new TodoService(todoRepository, userRepository);
    }

    @Test
    public void testInsertOrUpdateNoUser() {
        user.setId(2L);
        assertThrows(
                NoSuchElementException.class,
                () -> todoService.insertOrUpdate(todoItem));
    }

    @Test
    public void testInsertOrUpdateSuccess() {
        todoService.insertOrUpdate(todoItem);
        verify(todoRepository).save(todoItem);
    }
}