package edu.ktu.myuniproject.domain;

public enum TodoPriority {
    HIGH, MEDIUM, LOW
}
