package edu.ktu.myuniproject.service;

import edu.ktu.myuniproject.domain.TodoItem;
import edu.ktu.myuniproject.domain.User;
import edu.ktu.myuniproject.repository.TodoRepository;
import edu.ktu.myuniproject.repository.UserRepository;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    private final TodoRepository todoRepository;
    private final UserRepository userRepository;

    @Autowired
    public TodoService(TodoRepository todoRepository,
        UserRepository userRepository) {
        this.todoRepository = todoRepository;
        this.userRepository = userRepository;
    }

    public List<TodoItem> findAll() {
        return todoRepository.findAll();
    }

    public void insertOrUpdate(TodoItem item) {
        User user = userRepository.findById(item.getUser().getId()).orElseThrow(() ->
            new NoSuchElementException("User Does Not Exist"));
        item.setUser(user);
        todoRepository.save(item);
    }

    public void delete(Long id) {
        todoRepository.deleteById(id);
    }

}
