package edu.ktu.myuniproject.controllers;

import edu.ktu.myuniproject.Greeting;
import edu.ktu.myuniproject.domain.TodoItem;
import edu.ktu.myuniproject.domain.TodoPriority;
import edu.ktu.myuniproject.domain.User;
import edu.ktu.myuniproject.repository.UserRepository;
import edu.ktu.myuniproject.request.TodoItemRequest;
import edu.ktu.myuniproject.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {

    private static final String template = "Hello, %s %s!";

    private final TodoService service;

    @Autowired
    public TodoController(TodoService service) {
        this.service = service;
    }

    @GetMapping(value = "/hello/{firstname}/{surname}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Greeting greeting(@PathVariable(name="firstname") String name, @PathVariable(name="surname") String surname) {
        return new Greeting(String.format(template, name, surname));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TodoItem> all() {
        return service.findAll();
    }

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void save(@RequestBody TodoItemRequest request) {
        TodoItem item = new TodoItem(request.getContent(), request.getPriority(),
            new User(request.getUserId(), null));
        service.insertOrUpdate(item);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam(value="id") Long id) {
        service.delete(id);
    }

    @GetMapping(value = "/prepopulate", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TodoItem> prepopulate() {
        User user = new User(1L, null);
        service.insertOrUpdate(new TodoItem("Wash dishes", TodoPriority.HIGH, user));
        service.insertOrUpdate(new TodoItem("Have a nap", TodoPriority.MEDIUM, user));
        service.insertOrUpdate(new TodoItem("Stare at wall", TodoPriority.LOW, user));

        return service.findAll();
    }


}
