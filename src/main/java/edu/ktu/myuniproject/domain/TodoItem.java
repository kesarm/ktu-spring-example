package edu.ktu.myuniproject.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "todo")
public class TodoItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    public String content;
    @Enumerated(EnumType.STRING)
    public TodoPriority priority;
    @ManyToOne
    @JoinColumn(name="user_id")
    @JsonIgnore
    public User user;

    public TodoItem() {

    }

    public TodoItem(String content, TodoPriority priority, User user) {
        this.content = content;
        this.priority = priority;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public TodoPriority getPriority() {
        return priority;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPriority(TodoPriority priority) {
        this.priority = priority;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof TodoItem)) return false;
        TodoItem todo = (TodoItem) obj;

        return todo.id == id &&
                Objects.equals(todo.content, content) &&
                Objects.equals(todo.priority, priority);
    }

    @Override
    public int hashCode() {
        int result = Long.hashCode(id);
        result = result * 31 + content.hashCode();
        result = result * 31 + priority.hashCode();
        return result;
    }
}
