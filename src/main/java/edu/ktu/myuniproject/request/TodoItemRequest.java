package edu.ktu.myuniproject.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.ktu.myuniproject.domain.TodoPriority;
import java.util.Objects;

public class TodoItemRequest {
  public String content;
  public TodoPriority priority;
  public Long userId;

  @JsonCreator
  public TodoItemRequest(
      @JsonProperty("content") String content,
      @JsonProperty("priority") TodoPriority priority,
      @JsonProperty("userId") Long userId) {
    this.content = content;
    this.priority = priority;
    this.userId = userId;
  }

  public String getContent() {
    return content;
  }

  public TodoPriority getPriority() {
    return priority;
  }

  public Long getUserId() {
    return userId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TodoItemRequest todo = (TodoItemRequest) o;
    return todo.content.equals(content)
        && todo.priority.equals(priority)
        && todo.userId.equals(userId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(content, priority, userId);
  }
}
